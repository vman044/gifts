package net.jmdev.gifts.commands;

import net.jmdev.gifts.Gifts;
import net.jmdev.gifts.model.Gift;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

/*************************************************************************
 *
 * J&M CONFIDENTIAL - @author Viraj Prakash - 4/15/17 | 1:39 PM
 * __________________
 *
 *  [2017] J&M Plugin Development 
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of J&M Plugin Development and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to J&M Plugin Development
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from J&M Plugin Development.
 */
public class GiftCommand implements CommandExecutor {

	public boolean onCommand(CommandSender commandSender, Command command, String commandLabel, String[] args) {
		if (!(commandSender instanceof Player)) {
			Gifts.getInstance().println("You must be a player to execute this command!");
			return false;
		}
		Player player = (Player) commandSender;

		Gifts.getInstance().getGiftCreation().remove(player.getUniqueId());
		//Check if only one argument
		if (args.length == 1 && Bukkit.getOfflinePlayer(args[0]) != null) {

			int id = Gift.getNewId();
			//Try to find player
			String uuid;
			//Check if player is offline or online
			if (Bukkit.getPlayer(args[0]) == null) {
				//Player is offline
				OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);
				//Check if we can find their UUID
				if (offlinePlayer != null && offlinePlayer.hasPlayedBefore()) {
					//Offline player found, continue
					uuid = offlinePlayer.getUniqueId().toString();
				} else {
					//Offline Player not found, returns false.
					player.sendMessage(Gifts.getInstance().getConfig().getString("playerNotFoundMessage"));
					return false;
				}
			} else {
				//Player is online, found their UUID
				uuid = Bukkit.getPlayer(args[0]).getUniqueId().toString();
			}
			//Make gift, save it to config.yml
			Gift gift = new Gift(id, player.getUniqueId().toString(), uuid, false, null);
			Gift.saveToConfig(gift);
			Gifts.getInstance().getGiftCreation().put(player.getUniqueId(), id);

			//Open interface for player to put items
			Inventory inventory = Bukkit.createInventory(null, Gifts.getInstance().getConfig().getInt("slotNumber"), "Send Gift");
			player.openInventory(inventory);

		} else {
			//Typed in command wrong, send usage message.
			player.sendMessage(Gifts.getInstance().getConfig().getString("usageMessage"));
		}
		return false;
	}

}
