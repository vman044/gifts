package net.jmdev.gifts.commands;

import net.jmdev.gifts.Gifts;
import net.jmdev.gifts.model.Gift;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.UUID;

/*************************************************************************
 *
 * J&M CONFIDENTIAL - @author Viraj Prakash - 4/22/17 | 4:04 PM
 * __________________
 *
 *  [2017] J&M Plugin Development 
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of J&M Plugin Development and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to J&M Plugin Development
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from J&M Plugin Development.
 */
public class RedeemCommand implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String commandLabel, String[] args) {
		if (!(commandSender instanceof Player)) {
			Gifts.getInstance().println("You must be a player to execute this command!");
			return false;
		}
		Player player = (Player) commandSender;
		//Loop through all gifts available to the player
		for (Gift gift : Gift.getGifts(player.getUniqueId().toString())) {
			String uuid = gift.getFrom();
			//Create gift item and get its ItemMeta
			ItemStack giftItem = new ItemStack(Material.STORAGE_MINECART);
			ItemMeta giftItemMeta = giftItem.getItemMeta();

			String name;
			//Get sender's UUID
			if (Bukkit.getPlayer(UUID.fromString(uuid)) != null) {
				//Sender is online
				name = Bukkit.getPlayer(UUID.fromString(uuid)).getName();
			} else {
				//Sender is offline
				OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
				name = offlinePlayer.getName();
			}

			//Set name and lore of the item
			giftItemMeta.setDisplayName(Gifts.getInstance().getConfig().getString("giftItemName").replace("%player%", name));
			giftItemMeta.setLore(Arrays.asList(Gifts.getInstance().getConfig().getString("giftItemLore"), String.valueOf(gift.getId())));
			giftItem.setItemMeta(giftItemMeta);

			//Check if player's inventory has sufficient space
			if (!isFull(player.getInventory())) {
				player.getInventory().addItem(giftItem);
			} else {
				//If not, we drop it right underneath them
				player.getWorld().dropItemNaturally(player.getLocation(), giftItem);
			}
			//Set the gift as redeemed so that it can no longer be redeemed.
			gift.setRedeemed(true);
			Gift.saveToConfig(gift);
		}
		//Send the player a message that they have redeemed gifts.
		player.sendMessage(Gifts.getInstance().getConfig().getString("redeemMessage"));
		return false;
	}

	public boolean isFull(Inventory inventory) {
		for (ItemStack item : inventory.getContents()) {
			if (item == null) {
				return false;
			}
		}
		return true;
	}
}
