package net.jmdev.gifts.model;

import net.jmdev.gifts.Gifts;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/*************************************************************************
 *
 * J&M CONFIDENTIAL - @author Viraj Prakash - 4/15/17 | 1:55 PM
 * __________________
 *
 *  [2017] J&M Plugin Development 
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of J&M Plugin Development and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to J&M Plugin Development
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from J&M Plugin Development.
 */
public class Gift {

	private final int id;
	private final String from;
	private final String to;
	private boolean redeemed;
	private List<ItemStack> items = new ArrayList<>();

	/**
	 * Saves the gift to the config
	 * @param gift Gift to save
	 */
	public static void saveToConfig(Gift gift) {
		FileConfiguration config = Gifts.getInstance().getConfig();
		String path = "Gifts." + gift.getId() + ".";
		config.set(path + "from", gift.getFrom());
		config.set(path + "to", gift.getTo());
		config.set(path + "redeemed", gift.isRedeemed());
		config.set(path + "items", gift.getItems());
		Gifts.getInstance().saveConfig();
	}

	/**
	 * Gets all the gifts available to redeem for a player
	 * @param uuid Player's Unique ID
	 * @return List of Gifts to redeem
	 */
	public static List<Gift> getGifts(String uuid) {
		List<Gift> gifts = new ArrayList<>();
		FileConfiguration config = Gifts.getInstance().getConfig();
		for (String id : config.getConfigurationSection("Gifts").getKeys(false)) {
			String path = "Gifts." + id + ".";
			String from = config.getString(path + "from");
			String to = config.getString(path + "to");
			boolean redeemed = config.getBoolean(path + "redeemed");
			List<ItemStack> content = (List<ItemStack>) config.get(path + "items");
			if (!redeemed && content != null) {
				gifts.add(new Gift(Integer.parseInt(id), from, to, redeemed, content));
			}
		}
		return gifts;
	}

	/**
	 * Gets a gift by ID
	 * @param id ID of the gift
	 * @return the gift
	 */
	public static Gift getGift(int id) {
		FileConfiguration config = Gifts.getInstance().getConfig();
		if (config.getConfigurationSection("Gifts." + id) == null) return null;
		String path = "Gifts." + id + ".";
			String from = config.getString(path + "from");
			String to = config.getString(path + "to");
			boolean redeemed = config.getBoolean(path + "redeemed");
			List<ItemStack> content = ((List<ItemStack>) config.get(path + "items"));
			return new Gift(id, from, to, redeemed, content);
	}

	/**
	 * Finds a new ID to allocate to a gift
	 * @return ID
	 */
	public static int getNewId() {
		return Gifts.getInstance().getConfig().getConfigurationSection("Gifts").getKeys(false).size() + 1;
	}

	public Gift(int id, String from, String to, boolean redeemed, List<ItemStack> items) {
		this.id = id;
		this.from = from;
		this.to = to;
		this.redeemed = redeemed;
		this.items = items;
	}

	public int getId() {
		return id;
	}

	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}

	public boolean isRedeemed() {
		return redeemed;
	}

	public void setRedeemed(boolean redeemed) {
		this.redeemed = redeemed;
	}

	public List<ItemStack> getItems() {
		return items;
	}

	public void setItems(List<ItemStack> items) {
		this.items = items;
	}

}
