package net.jmdev.gifts.listeners;

import net.jmdev.gifts.Gifts;
import net.jmdev.gifts.model.Gift;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

/*************************************************************************
 *
 * J&M CONFIDENTIAL - @author Viraj Prakash - 4/15/17 | 9:28 PM
 * __________________
 *
 *  [2017] J&M Plugin Development 
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of J&M Plugin Development and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to J&M Plugin Development
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from J&M Plugin Development.
 */
public class GiftListener implements Listener {

	@EventHandler
	public void onClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();

		//Check if they are in the interface to send a gift
		if (e.getInventory().getTitle().contains("Send Gift")) {

			//Find the Gift they are creating
			int id = Gifts.getInstance().getGiftCreation().get(player.getUniqueId());
			Gift gift = Gift.getGift(id);

			//Check if the inventory is empty
			if (isEmpty(e.getInventory())) {
				gift.setRedeemed(true);
				Gift.saveToConfig(gift);
				return;
			}

			//Add all of the items in the inventory to a new list
			List<ItemStack> items = new ArrayList<>();
			Arrays.stream(e.getInventory().getContents()).filter(Objects::nonNull).forEach(items::add);

			//Save the items in the gift
			gift.setItems(items);
			Gift.saveToConfig(gift);

			//Check if the player is online
			if (Bukkit.getPlayer(UUID.fromString(gift.getTo())) != null) {
				//Player is online, so send them a message
				Player to = Bukkit.getPlayer(UUID.fromString(gift.getTo()));
				to.sendMessage(Gifts.getInstance().getConfig().getString("receivedGiftMessage").replace("%player%", player.getName()));
			}
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		//Loop through all the gifts available for the player to redeem
		for (Gift gift : Gift.getGifts(e.getPlayer().getUniqueId().toString())) {
			//Find the name of the sender
			String name;
			String uuid = gift.getFrom();
			if (Bukkit.getPlayer(UUID.fromString(uuid)) != null) {
				name = Bukkit.getPlayer(UUID.fromString(uuid)).getName();
			} else {
				OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
					name = offlinePlayer.getName();
			}
			//Send the player a message that they have received a gift
			player.sendMessage(Gifts.getInstance().getConfig().getString("receivedGiftMessage").replace("%player%", name));
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerInteract(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		//Check if they right clicked
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			//They right clicked, check if they had a storage minecart in their hand
			if (e.getPlayer().getItemInHand().getType() == Material.STORAGE_MINECART) {
				ItemStack item = player.getInventory().getItemInHand();
				//Has an Item Meta
				if (item.hasItemMeta()) {
					ItemMeta meta = item.getItemMeta();
					//99% sure by this point it's a gift, but let's just make sure the lore matches
					if (meta.getLore().size() >= 2) {
						try {
							//Get the gift's ID
							int id = Integer.parseInt(meta.getLore().get(1));
							//Make sure the gift is not null
							if (Gift.getGift(id) != null) {
								//Get the gift, the sender, and the items within the gift
								Gift gift = Gift.getGift(id);
								String name;
								String uuid = gift.getFrom();
								if (Bukkit.getPlayer(UUID.fromString(uuid)) != null) {
									name = Bukkit.getPlayer(UUID.fromString(uuid)).getName();
								} else {
									OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(UUID.fromString(uuid));
									name = offlinePlayer.getName();
								}
								//Remove the Gift Item from their inventory
									Bukkit.getScheduler().runTaskLater(Gifts.getInstance(), () -> player.getInventory().remove(item), 1);

								//Create inventory with items
									Inventory inv = Bukkit.createInventory(null,
											Gifts.getInstance().getConfig().getInt("slotNumber"),
											Gifts.getInstance().getConfig().getString("giftItemName").replace("%player%", name));
									gift.getItems().forEach(inv::addItem);

								//Open the inventory
									player.openInventory(inv);
							}
						} catch (NumberFormatException exception) {

						}
					}
				}
			}
		}
	}

	public boolean isEmpty(Inventory inventory) {
		for (ItemStack item : inventory.getContents()) {
			if (item != null) {
				return false;
			}
		}
		return true;
	}

}
