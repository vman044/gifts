package net.jmdev.gifts;

import net.jmdev.gifts.commands.GiftCommand;
import net.jmdev.gifts.commands.RedeemCommand;
import net.jmdev.gifts.listeners.GiftListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

/*************************************************************************
 *
 * J&M CONFIDENTIAL - @author Viraj Prakash - 4/15/17 | 1:36 PM
 * __________________
 *
 *  [2017] J&M Plugin Development 
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of J&M Plugin Development and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to J&M Plugin Development
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from J&M Plugin Development.
 */
public class Gifts extends JavaPlugin {

	private static Gifts plugin;
	private Logger logger;
	private Map<UUID, Integer> giftCreation = new HashMap<>();

	public static Gifts getInstance() {
		return plugin;
	}

	public Map<UUID, Integer> getGiftCreation() {
		return giftCreation;
	}

	@Override
	public void onEnable() {
		logger = Bukkit.getLogger();
		plugin = this;

		//Load config
		getConfig().options().copyDefaults(true);
		saveConfig();

		//Load commands
		getCommand("gift").setExecutor(new GiftCommand());
		getCommand("redeem").setExecutor(new RedeemCommand());

		//Load listener
		getServer().getPluginManager().registerEvents(new GiftListener(), this);

		println("has been enabled.");
	}

	@Override
	public void onDisable() {
		println("has been disabled.");
	}


	public void println(String msg) {
		logger.info("[Gifts] " + msg);
	}

}
